# -*- coding: utf-8 -*-

#******************************************************************************
#
# QMetaTiles
# ---------------------------------------------------------
# Generates tiles (using metatiles) from a QGIS project
#
# Copyright (C) 2015 we-do-IT (info@we-do-it.com)
# Copyright (C) 2012-2014 NextGIS (info@nextgis.org)
#
# This source is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation, either version 2 of the License, or (at your option)
# any later version.
#
# This code is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# A copy of the GNU General Public License is available on the World Wide Web
# at <http://www.gnu.org/licenses/>. You can also obtain it by writing
# to the Free Software Foundation, 51 Franklin Street, Suite 500 Boston,
# MA 02110-1335 USA.
#
#******************************************************************************

import locale
import math
import operator
from qgis.PyQt.QtCore import QSettings, QDir, QFileInfo, Qt, pyqtSlot
#from qgis.PyQt.QtGui import *
from qgis.PyQt.QtWidgets import QDialog, QFileDialog, QMessageBox, QDialogButtonBox
from qgis.core import QgsCoordinateTransform, QgsCoordinateReferenceSystem, QgsRectangle, QgsProject, QgsSettings
from .tilingthread import TilingThread
from .ui.ui_qtilesdialogbase import Ui_Dialog
from .qtiles_utils import getMapLayers, getLayerById, getLayerGroup
import os

class QTilesDialog(QDialog, Ui_Dialog):
    def __init__(self, iface):
        QDialog.__init__(self)
        self.setupUi(self)
        self.iface = iface
        self.workThread = None
        self.FORMATS = {
            self.tr('ZIP archives (*.zip *.ZIP)') : '.zip',
            self.tr('MBTiles databases (*.mbtiles *.MBTILES)') : '.mbtiles'}
        self.settings = QgsSettings('we-do-IT', 'QMetaTiles')
        self.grpParameters.setSettings(self.settings)
        self.btnOk = self.buttonBox.button(QDialogButtonBox.Ok)
        self.btnClose = self.buttonBox.button(QDialogButtonBox.Close)
        self.rbOutputZip.toggled.connect(self.__toggleZipTarget)
        self.rbExtentLayer.toggled.connect(self.__toggleLayerSelector)
        self.chkLockRatio.stateChanged.connect(self.__toggleHeightEdit)
        self.spnTileWidth.valueChanged.connect(self.__updateTileSize)
        self.btnBrowse.clicked.connect(self.__selectOutput)
        self.cmbFormat.activated.connect(self.__formatChanged)
        self.chkUseMetatiling.toggled.connect(self.__metaTilingChanged)
        self.manageGui()

    def manageGui(self):
        layers = getMapLayers()
        root = QgsProject.instance().layerTreeRoot()
        for layer in sorted(layers.items(), key=operator.itemgetter(1)):
            node = root.findLayer(layer[0])
            if node.parent() == root:
                self.cmbLayers.addItem(layer[1], layer[0])
            else:
                self.cmbLayers.addItem('%s - %s' % (layer[1], node.parent().name()), layer[0])
        self.rbOutputZip.setChecked(self.settings.value('outputToZip', True, type=bool))
        self.rbOutputDir.setChecked(self.settings.value('outputToDir', False, type=bool))
        if self.rbOutputZip.isChecked():
            self.leDirectoryName.setEnabled(False)
        else:
            self.leZipFileName.setEnabled(False)
        self.cmbLayers.setEnabled(False)
        self.leRootDir.setText(self.settings.value('rootDir', 'Mapnik'))
        self.rbExtentCanvas.setChecked(self.settings.value('extentCanvas', True, type=bool))
        self.rbExtentFull.setChecked(self.settings.value('extentFull', False, type=bool))
        self.rbExtentLayer.setChecked(self.settings.value('extentLayer', False, type=bool))
        self.spnZoomMin.setValue(self.settings.value('minZoom', 0, type=int))
        self.spnZoomMax.setValue(self.settings.value('maxZoom', 18, type=int))
        self.chkLockRatio.setChecked(self.settings.value('keepRatio', True, type=bool))
        self.spnTileWidth.setValue(self.settings.value('tileWidth', 256, type=int))
        self.spnTileHeight.setValue(self.settings.value('tileHeight', 256, type=int))
        self.spnTransparency.setValue(self.settings.value('transparency', 100, type=int))
        self.spnQuality.setValue(self.settings.value('quality', 70, type=int))
        self.cmbFormat.setCurrentIndex(int(self.settings.value('format', 0)))
        self.chkAntialiasing.setChecked(self.settings.value('enable_antialiasing', False, type=bool))
        self.chkTMSConvention.setChecked(self.settings.value('use_tms_filenames', False, type=bool))
        self.chkWriteMapurl.setChecked(self.settings.value("write_mapurl", False, type=bool))
        self.chkWriteViewer.setChecked(self.settings.value("write_viewer", False, type=bool))
        self.chkUseMetatiling.setChecked(self.settings.value("use_metatiling", True, type=bool))
        self.chkBufferMetatiles.setChecked(self.settings.value("use_buffer", True, type=bool))
        self.spnMetaWidth.setValue(self.settings.value("meta_width", 2, type=int))
        self.spnMetaHeight.setValue(self.settings.value("meta_height", 2, type=int))
        self.chkFeatures.setChecked(self.settings.value("llg_features", False, type=bool))

        self.__formatChanged()
        self.__metaTilingChanged()

    def reject(self):
        QDialog.reject(self)

    def accept(self):
        
        if self.rbOutputZip.isChecked():
            output = self.leZipFileName.text()
        else:
            output = self.leDirectoryName.text()
        if not output:
            QMessageBox.warning(self, self.tr('No output'), self.tr('Output path is not set. Please enter correct path and try again.'))
            return
        fileInfo = QFileInfo(output)
        if fileInfo.isDir() and not len(QDir(output).entryList(QDir.Dirs | QDir.Files | QDir.NoDotAndDotDot)) == 0:
            res = QMessageBox.warning(self, self.tr('Directory not empty'), self.tr('Selected directory is not empty. Continue?'), QMessageBox.Yes | QMessageBox.No)
            if res == QMessageBox.No:
                return
        if self.spnZoomMin.value() > self.spnZoomMax.value():
            QMessageBox.warning(self, self.tr('Wrong zoom'), self.tr('Maximum zoom value is lower than minimum. Please correct this and try again.'))
            return
        self.settings.setValue('rootDir', self.leRootDir.text())
        self.settings.setValue('outputToZip', self.rbOutputZip.isChecked())
        self.settings.setValue('outputToDir', self.rbOutputDir.isChecked())
        self.settings.setValue('extentCanvas', self.rbExtentCanvas.isChecked())
        self.settings.setValue('extentFull', self.rbExtentFull.isChecked())
        self.settings.setValue('extentLayer', self.rbExtentLayer.isChecked())
        self.settings.setValue('minZoom', self.spnZoomMin.value())
        self.settings.setValue('maxZoom', self.spnZoomMax.value())
        self.settings.setValue('keepRatio', self.chkLockRatio.isChecked())
        self.settings.setValue('tileWidth', self.spnTileWidth.value())
        self.settings.setValue('tileHeight', self.spnTileHeight.value())
        self.settings.setValue('format', self.cmbFormat.currentIndex ())
        self.settings.setValue('transparency', self.spnTransparency.value())
        self.settings.setValue('quality', self.spnQuality.value())
        self.settings.setValue('enable_antialiasing', self.chkAntialiasing.isChecked())
        self.settings.setValue('use_tms_filenames', self.chkTMSConvention.isChecked())
        self.settings.setValue('write_mapurl', self.chkWriteMapurl.isChecked())
        self.settings.setValue('write_viewer', self.chkWriteViewer.isChecked())
        self.settings.setValue('use_metatiling', self.chkUseMetatiling.isChecked())
        self.settings.setValue('use_buffer', self.chkBufferMetatiles.isChecked())
        self.settings.setValue('meta_width', self.spnMetaWidth.value())
        self.settings.setValue('meta_height', self.spnMetaHeight.value())
        self.settings.setValue('llg_features', self.chkFeatures.isChecked())

        canvas = self.iface.mapCanvas()
        if self.rbExtentCanvas.isChecked():
            extent = canvas.extent()
        elif self.rbExtentFull.isChecked():
            extent = canvas.fullExtent()
        else:
            layer = getLayerById(self.cmbLayers.itemData(self.cmbLayers.currentIndex()))
            try:
                extent = canvas.mapSettings().layerExtentToOutputExtent(layer, layer.extent())
            except:
                QMessageBox.critical(self, self.tr('Invalid layer'), self.tr('A vector layer must be selected to use this Extent option.'))
                return
        extent = QgsCoordinateTransform(canvas.mapSettings().destinationCrs(), QgsCoordinateReferenceSystem('EPSG:4326'), QgsProject.instance()).transform(extent)
        arctanSinhPi = math.degrees(math.atan(math.sinh(math.pi)))
        extent = extent.intersect(QgsRectangle(-180, -arctanSinhPi, 180, arctanSinhPi))
        layers = canvas.mapSettings().layers()
        writeMapurl = self.chkWriteMapurl.isEnabled() and self.chkWriteMapurl.isChecked()
        writeViewer = self.chkWriteViewer.isEnabled() and self.chkWriteViewer.isChecked()
        metatile = self.chkUseMetatiling.isChecked()
        metatile_buffer = self.chkBufferMetatiles.isChecked()
        metatile_size = {'rows': self.spnMetaHeight.value(), 'cols': self.spnMetaWidth.value()}
        llg_features = self.chkFeatures.isChecked()
        self.workThread = TilingThread( layers,
                                        extent,
                                        self.spnZoomMin.value(),
                                        self.spnZoomMax.value(),
                                        self.spnTileWidth.value(),
                                        self.spnTileHeight.value(),
                                        self.spnTransparency.value(),
                                        self.spnQuality.value(),
                                        self.cmbFormat.currentText(),
                                        fileInfo,self.leRootDir.text(),
                                        self.chkAntialiasing.isChecked(),
                                        self.chkTMSConvention.isChecked(),
                                        writeMapurl,
                                        writeViewer,
                                        metatile,
                                        metatile_size,
                                        metatile_buffer,
                                        llg_features
                                        )

        self.workThread.rangeChanged.connect(self.setProgressRange)
        self.workThread.updateProgress.connect(self.updateProgress)
        self.workThread.processFinished.connect(self.processFinished)
        self.workThread.processInterrupted.connect(self.processInterrupted)

        self.btnOk.setEnabled(False)
        self.btnClose.setText(self.tr('Cancel'))
        self.buttonBox.rejected.disconnect(self.reject)
        self.btnClose.clicked.connect(self.stopProcessing)
        self.groupBox.setEnabled(False)
        self.groupBox_2.setEnabled(False)
        self.groupBox_3.setEnabled(False)
        self.grpMetaTiling.setEnabled(False)
        self.grpParameters.setEnabled(False)

        self.workThread.start()

        ##  this does the same thing but without using a seperate thread. Good for debugging.
        #from tileset import TileSet
        #no_thread = TileSet(layers,extent,self.spnZoomMin.value(),self.spnZoomMax.value(),self.spnTileWidth.value(),self.spnTileHeight.value(),
        #                                            self.spnTransparency.value(),self.spnQuality.value(),self.cmbFormat.currentText(),fileInfo,self.leRootDir.text(),
        #                                            self.chkAntialiasing.isChecked(),self.chkTMSConvention.isChecked(),writeMapurl,writeViewer,metatile,metatile_size,
        #                                            metatile_buffer,llg_features)
        #no_thread.run()

    def setProgressRange(self, message, value):
        self.progressBar.setFormat(message)
        self.progressBar.setRange(0, value)

    def updateProgress(self):
        self.progressBar.setValue(self.progressBar.value() + 1)

    def processInterrupted(self):
        self.restoreGui()

    def processFinished(self):
        self.stopProcessing()
        self.restoreGui()

    def stopProcessing(self):
        if self.workThread is not None:
            self.workThread.stop()
            self.workThread = None

    def restoreGui(self):
        self.progressBar.setFormat('%p%')
        self.progressBar.setRange(0, 1)
        self.progressBar.setValue(0)
        self.buttonBox.rejected.connect(self.reject)
        self.btnClose.clicked.disconnect(self.stopProcessing)
        self.btnClose.setText(self.tr('Close'))
        self.btnOk.setEnabled(True)
        self.groupBox.setEnabled(True)
        self.groupBox_2.setEnabled(True)
        self.groupBox_3.setEnabled(True)
        self.grpMetaTiling.setEnabled(True)
        self.grpParameters.setEnabled(True)

    def __toggleZipTarget(self, checked):
        self.leZipFileName.setEnabled(checked)
        self.leDirectoryName.setEnabled(not checked)
        self.chkWriteMapurl.setEnabled(not checked)
        self.chkWriteViewer.setEnabled(not checked)
        self.chkFeatures.setEnabled(not checked)

    def __toggleLayerSelector(self, checked):
        self.cmbLayers.setEnabled(checked)

    def __toggleHeightEdit(self, state):
        if state == Qt.Checked:
            self.lblHeight.setEnabled(False)
            self.spnTileHeight.setEnabled(False)
            self.spnTileHeight.setValue(self.spnTileWidth.value())
        else:
            self.lblHeight.setEnabled(True)
            self.spnTileHeight.setEnabled(True)

    def __formatChanged(self):
        if self.cmbFormat.currentText()=='JPG':
            self.spnTransparency.setEnabled(False)
            self.spnQuality.setEnabled(True)
        else:
            self.spnTransparency.setEnabled(True)
            #self.spnQuality.setEnabled(False) #  quality does effect the size of png files!! 70 is good

    def __metaTilingChanged(self):
        if self.chkUseMetatiling.isChecked():
            self.chkBufferMetatiles.setEnabled(True)
            self.label_9.setEnabled(True)
            self.label_10.setEnabled(True)
            self.spnMetaHeight.setEnabled(True)
            self.spnMetaWidth.setEnabled(True)
        else:
            self.chkBufferMetatiles.setEnabled(False)
            self.label_9.setEnabled(False)
            self.label_10.setEnabled(False)
            self.spnMetaHeight.setEnabled(False)
            self.spnMetaWidth.setEnabled(False)

    @pyqtSlot(int)
    def __updateTileSize(self, value):
        if self.chkLockRatio.isChecked():
            self.spnTileHeight.setValue(value)

    def __selectOutput(self):
        lastDirectory = self.settings.value('lastUsedDir', '.')
        if self.rbOutputZip.isChecked():
            outPath, outFilter = QFileDialog.getSaveFileName(self, self.tr('Save to file'), lastDirectory,  ';;'.join(list(self.FORMATS.keys())), list(self.FORMATS.keys())[list(self.FORMATS.values()).index('.zip')])
            if not outPath:
                return
            if not outPath.lower().endswith(self.FORMATS[outFilter]):
                outPath += self.FORMATS[outFilter]
            print (outPath)
            self.leZipFileName.setText(outPath)
        else:
            outPath = QFileDialog.getExistingDirectory(self, self.tr('Save to directory'), lastDirectory, QFileDialog.ShowDirsOnly)
            if not outPath:
                return
            self.leDirectoryName.setText(outPath)
        self.settings.setValue('lastUsedDir', QFileInfo(outPath).absoluteDir().absolutePath())
